CFLAGS = -Wall -g

all: client server

.c.o:
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -f server client
