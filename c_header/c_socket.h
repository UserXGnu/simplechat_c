#ifndef _C_SOCKET_H_
#define _C_SOCKET_H_

#include "../headers/d_socket.h"

#include <netdb.h>

typedef struct hostent host_t;

void Connect (int sockfd, const SA * addr, socklen_t addrlen) {

    int ready;
    ready = connect(sockfd, addr, addrlen);
    
    if (ready < 0) {
        
        err_quit("in connect()");
        
    }

    fprintf (stdout, "[%s+%s] connect() is ok ...\n", T_BLUE, NOTHING);
}


#endif /* _C_SOCKET_H_ */
