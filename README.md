simplechat_c
============

A simple socket chat in C

 This program was done with hope that it may help some people 
interested in the programming language C and how to use sockets
with it.
Obviously, I wish to receive some warnings and lessons about
how to improve and be better programmer that I am now.

(actually, i'm not even a good programmer, I think).






License:
========

Copyright (C) 2013 - UserX (Victor Vigel Flores)


This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
