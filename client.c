
#include "c_header/c_socket.h"

#define NICKLEN 16
#define MAXLINE 1024
#define max(a,b) ((a)>(b)?(a):(b))

void connection (int * argc, char ** argv) {
    
    int n;
    int sockfd;
    int maxfd;
    int sendlen;
    int recvlen;
    
    extern int h_errno;
    
    char buffer [MAXLINE];
    char msg [MAXLINE];
    char nickname [NICKLEN];
    
    SA_IN host_addr;
    host_t * he;
    
    socklen_t sin_size = sizeof(SA_IN);
    fd_set rset;
    
    if (* argc != 3) {
    
        fprintf(stderr, "Usage: %s <ip address> <port>\n", argv[0]);
        exit(1);
        
    } 
    
    fprintf(stdout, "Type a Nick: ");
    fgets(nickname, NICKLEN, stdin);
    nickname[strlen(nickname) - 1] = 0x00;
    
    printf("\n\t[%sClient%s] - %sStarting connection%s [%s!!%s]\t\n",
            T_YELL, NOTHING, T_YELL, NOTHING, T_YELL, NOTHING);
            
       
    sockfd = Socket (AF_INET, SOCK_STREAM, 0);
    
    bzero (&(host_addr), sizeof (host_addr) );
    host_addr.sin_family = AF_INET;
    host_addr.sin_port = htons(atoi(argv[2]));
    

    if ( (n = inet_pton (AF_INET, argv[1], &host_addr.sin_addr) ) == 0 ) {
    
        he = gethostbyname(argv[1]);
        
        if (he == NULL) {
        
            if (h_errno == HOST_NOT_FOUND) {
            
                fprintf(stderr, "[%sCLIENT%s] Error: Especified host %s%s%s is %sunknown%s  [%s!!%s]\n",
                        T_YELL, NOTHING, T_RED, argv[1], NOTHING, T_BLUE, NOTHING, T_YELL, NOTHING);
                exit (-1);
            
            }
            
            host_addr.sin_addr = * ((struct in_addr * )he->h_addr);
            
        }
        
    }else if (n < 0) {
    
        err_quit("in inet_pton ()");
    
    }
    
    Connect(sockfd, (SA *)&host_addr, sin_size);

    printf("\n\t[%sWARNING%s] Connection successfuly with host %s%s%s port: %s%d%s\n",
            T_BLUE, NOTHING, T_BLUE, inet_ntoa(host_addr.sin_addr), NOTHING, T_BLUE, atoi(argv[2]),
             NOTHING);
             
   FD_ZERO(&rset);
   
   for (;;) {
  
        FD_SET(sockfd, &rset);
        FD_SET(fileno(stdin), &rset);
        
        maxfd = max(sockfd, fileno(stdin));
   
        select (maxfd + 1, &rset, NULL, NULL, NULL);
        
        if (FD_ISSET(fileno(stdin), &rset)) { 
        
           fgets(buffer, MAXLINE, stdin);
           sprintf(msg, "[%s]: %s", nickname, buffer);
           sendlen = send(sockfd, msg, strlen(msg), 0);
           bzero (&(buffer), sizeof (buffer) );
//           bzero (&(msg), sizeof(msg));
        
        } else if (FD_ISSET(sockfd, &rset)) {
        
            recvlen = recv(sockfd, buffer, MAXLINE, 0);
            if(recvlen < 0) {
                printf("\n\t[%sSERVER%s] %sWARNING%s: Connection can't be completely received [%s!!%s]\t\n",
                        T_YELL, NOTHING, T_RED, NOTHING, T_RED, NOTHING);
                        err_quit("in recv()");
                        
            }else if (recvlen == 0) {
                
               printf("\n\t[%sSERVER%s] %sWARNING%s: SERVER is not online anymore [%s!!%s]\t\n",
                        T_YELL, NOTHING, T_RED, NOTHING, T_RED, NOTHING);
                        exit(1);
            }else {
                printf("%s", buffer);
                bzero (&(buffer), sizeof (buffer ));
            }
        
        }
        
   }
}

int main (int argc, char ** argv) {

    connection (&argc, argv);
    
    
    return 0;

}



