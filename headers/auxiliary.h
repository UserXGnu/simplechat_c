#ifndef _AUXILIARY_H_
#define _AUXILIARY_H_

#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <string.h>
#include <errno.h>

#include "color_set.h"

void err_quit (char * message) {
    char error [150];

    sprintf(error, "[%s-%s] SERVER WARNING - ", T_RED, NOTHING);
    
    if( (message != NULL) && (strlen(message) <= 100) ) {
        
        strncat(error, message, (150 - strlen(error)) );
         
    }
    
    perror(error);
    exit(-1);

}

extern void clear(void)
{
        printf("\e[H\e[2J");
}

#endif /* _AUXILIARY_H_ */
