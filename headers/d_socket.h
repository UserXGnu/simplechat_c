#ifndef _D_SOCKET_H_
#define _D_SOCKET_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <string.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include "auxiliary.h"

#ifndef TV
#define TV struct timeval
#endif


#ifndef SA
#define SA  struct sockaddr
#endif

#ifndef SA_IN
#define SA_IN   struct sockaddr_in
#endif


int Socket (int family, int type, int protocol) {

    int sockfd;
    int yes = 1;
    sockfd = socket (family, type, protocol);
    
    if (sockfd < 0) {
    
        err_quit ("in socket()");
    
    }
    fprintf(stdout, "[%s+%s] socket() is ok ...\n", T_BLUE, NOTHING);
    
    
    if((setsockopt (sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) ) < 0) {
    
        err_quit("in setsockopt()");
    
    }
    
    fprintf(stdout, "[%s+%s] setsockopt() is ok ...\n", T_BLUE, NOTHING);
    
    return sockfd;
}



#endif /* _D_SOCKET_H_ */


