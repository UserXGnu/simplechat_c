#ifndef _MS_SERVER_H_
#define _MS_SERVER_H_

#include "s_socket.h"

#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>


#ifndef PORT
#define PORT    54321
#endif


#ifndef BACKLOG
#define BACKLOG 32
#endif


#ifndef NICKLEN
#define NICKLEN 16
#endif


#ifndef MAXLINE
#define MAXLINE 512
#endif


#ifndef MAXCLI
#define MAXCLI  9
#endif 


#ifndef SERVER
#define SERVER  0
#endif


#define max(a,b) ((a)>(b)?(a):(b))

void cli_mplex (int sock_v[], fd_set * rset, int * maxfd);
int maxfd_calculate (int sock_v[]);

void
init (int * argc, char ** argv)
{

    int sockfd;
    int sock_v [MAXCLI];
 
    int lmaxfd;
    int rmaxfd;
    int counter;
    int selected;
    int i;
    extern int h_errno;
    
    char buffer[MAXLINE];
    
    SA_IN host_addr;
    SA_IN cli_addr;
    
    TV tv;
    
    socklen_t sin_size;
    fd_set lset;
    fd_set rset;
    
    sockfd = Socket (AF_INET, SOCK_STREAM, 0);
    
    bzero (&host_addr, sizeof(SA_IN));
    host_addr.sin_family = AF_INET;
    host_addr.sin_port = htons (PORT);
    host_addr.sin_addr.s_addr = htonl (INADDR_ANY);
    
    Bind (sockfd, (SA *)&host_addr, sizeof(host_addr));
    
    Listen (sockfd, MAXCLI);
    
    for (i = 0; i < MAXCLI; i++) {
    
        sock_v[i] = -2;
        
    }
    
    counter = 0;
    
    tv.tv_sec = 0;
    tv.tv_usec = 0;
    
    FD_ZERO (&lset);
    FD_ZERO (&rset);
   
    for ( ;; ) {
    
        FD_SET(sockfd, &lset);
        FD_SET(fileno(stdin), &lset);
        lmaxfd = (max (fileno(stdin), sockfd)) + 1;
        
        selected = select ( lmaxfd, &lset, NULL, NULL, &tv);

        if (FD_ISSET(fileno(stdin), &lset)) {
        
            fgets(buffer, sizeof (buffer), stdin);
            for (i = 0; i < MAXCLI; i++) {
            
                if (sock_v[i] != -2) {
                
                    write(sock_v[i], buffer, strlen(buffer) );
                
                }
            
            }
            bzero (&buffer, sizeof (buffer) );
            
        } 
        if(FD_ISSET(sockfd, &lset)) {

            for (i = 0; i < MAXCLI; i ++) {
            
                if (sock_v[i] == -2)
                    break;
            
            }
            
            sin_size = sizeof(SA_IN);
            sock_v[i] = Accept (sockfd, (SA *)&cli_addr, &sin_size);
            
            counter ++;
            
        }
        
        if (counter > 0 ) {
          
            cli_mplex (sock_v, &rset, &rmaxfd);
          
        }
    
    }
    
}


void
cli_mplex (int sock_v[], fd_set * rset, int * maxfd)
{

    int i;
    int j;
    
    int data_l;
    int selected;
    char buffer[MAXLINE];
    TV tv;
    
    for (i = 0; i < MAXCLI; i++) {
    
        if (sock_v[i] != -2) {
        
            FD_SET(sock_v[i], rset);
        
        }
    
    }
    
    *maxfd = maxfd_calculate (sock_v);
    
    bzero(&buffer, sizeof (buffer));
    
    tv.tv_sec = 0;
    tv.tv_usec = 0;
    
    selected = select (*maxfd, rset, NULL, NULL, &tv);    
    
    
    for (i = 0; i < MAXCLI; i++) {
        
        if (sock_v[i] != -2){
    
            if (FD_ISSET(sock_v[i], rset)) {
                printf("[SERVER]: data comming from %d client [!!]\n", sock_v[i]);
                data_l = read (sock_v[i], buffer, sizeof (buffer));
                if (data_l < 0) {
                
                    perror ("in read ()");
                    
                } else if (data_l == 0) {
                
                    printf("[SERVER]: connection closed by remote host %d [!!]\n", sock_v[i]);
                    
                    FD_CLR(sock_v[i], rset);
                    close(sock_v[i]);
                    sock_v[i] = -2;
                
                } else {
                    printf("[SERVER]: data has come from %d client [!!]\n%s\n", sock_v[i], buffer);
                    for (j = 0; j < MAXCLI; j++) {
                        
                        if (sock_v[j] != -2) {
                            
                            write (sock_v[j], buffer, strlen(buffer));
    
                        
                        }
                        
                    }
                    
                    bzero (&buffer, sizeof(buffer));
                    
                }
            
            }
            
        }
    
    }
    

}

int
maxfd_calculate (int sock_v[]) {

    int higher;
    int i;
    
    higher = sock_v[0];
    
    for (i = 0; i < MAXCLI; i++) {
 
        higher = max (higher, sock_v[i]);
    
    }

    return (higher+1);
    
}


#endif /* _MS_SERVER_H_ */
