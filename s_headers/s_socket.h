#ifndef _S_SOCKET_H_
#define _S_SOCKET_H_

#include "../headers/d_socket.h"

#ifndef BACKLOG
#define BACKLOG     32
#endif

void Bind (int sockfd, const SA * addr, socklen_t addrlen) {

    int ready;
    ready = bind (sockfd, addr, addrlen);
    
    if (ready < 0) {
    
        err_quit ("in bind()");
    
    }
    
    fprintf(stdout,"[%s+%s] bind() is ok ...\n", T_BLUE, NOTHING);
    
}

void Listen (int sockfd, int backlog) {

    int ready;
    ready = listen (sockfd, backlog);
    
    if(ready < 0) {
        
        err_quit ("in listen()");
        
    }
    
    fprintf(stdout,"[%s+%s] listen() is ok ...\n", T_BLUE, NOTHING);
    
}

int Accept (int sockfd, SA * addr, socklen_t * addrlen) {

    int newsock;
    newsock = accept (sockfd, addr, addrlen);
    
    if(newsock < 0) {
        
        err_quit ("in accept()");
        
    }
    
    fprintf(stdout,"[%s+%s] accept() is ok ...\n", T_BLUE, NOTHING);
    
    return newsock;
    
}

#endif /* _S_SOCKET_H_ */
